package com.sabre.demo;

import com.sabre.fltsked.extensions.modules.profit.ds.DayOfWeek;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CodeshareConnectionEditingTest {

    private static final String DEST_ARP = "DFW";
    private static final String ARVL_ARP = "ORD";
    private static final Frequency ALL_FREQUENCY = Frequency.create(Arrays.asList(DayOfWeek.values()));
    private final Itinerary pouiItineraryOperatorToCodeshareToCodeshare = buildPOUIItineraryOperatorToCodeshareToCodeshare();
    private final Itinerary pouiItineraryCodeshareToOperatorCodeshare = buildPOUIItineraryCodeshareToOperatorCodeshare();

    @Before
    public void setUp() {
    }

    @Ignore
    @Test
    public void testGetValidFrequencies() {
        List<Itinerary> itineraries = new LinkedList<Itinerary>();
        itineraries.add(pouiItineraryOperatorToCodeshareToCodeshare);
        itineraries.add(pouiItineraryCodeshareToOperatorCodeshare);

        CodeshareConnectionEditing testObj = new CodeshareConnectionEditing();
        Map<Itinerary, Frequency> result = testObj.getValidFrequencies(DEST_ARP, ARVL_ARP, itineraries);
        assertEquals(Frequency.create(), result.get(pouiItineraryOperatorToCodeshareToCodeshare));
        assertEquals(Frequency.create(), result.get(pouiItineraryCodeshareToOperatorCodeshare));
    }

    private Itinerary buildPOUIItineraryOperatorToCodeshareToCodeshare() {
        List<FlightLeg> legs = new LinkedList<FlightLeg>();
        legs.add(leg("UA", true, ""));
        legs.add(leg("UA", false, "EY"));
        legs.add(leg("UA", false, "AA"));
        return itinerary(ALL_FREQUENCY, legs);
    }

    private Itinerary buildPOUIItineraryCodeshareToOperatorCodeshare() {
        List<FlightLeg> legs = new LinkedList<FlightLeg>();
        legs.add(leg("AS", false, "AA"));
        legs.add(leg("AS", true, ""));
        legs.add(leg("AS", false, "DL"));
        return itinerary(ALL_FREQUENCY, legs);
    }

    static Itinerary itinerary(final Frequency pFreq, final List<FlightLeg> pLegs) {
        return new Itinerary() {
            @Override
            public Frequency getFrequency() {
                return pFreq;
            }

            @Override
            public String getDeptArp() {
                return DEST_ARP;
            }

            @Override
            public String getArvlArp() {
                return ARVL_ARP;
            }

            @Override
            public int getDistance() {
                return 0;
            }

            @Override
            public int getDeptTime() {
                return 0;
            }

            @Override
            public int getArvlTime() {
                return 0;
            }

            @Override
            public int getElapsedTime() {
                return 0;
            }

            @Override
            public List<FlightLeg> getLegs() {
                return pLegs;
            }
        };
    }

    static FlightLeg leg(final String pAirline, final boolean pIsOperational, final String pOperationalAirline) {
        return new FlightLeg() {
            @Override
            public String getAirline() {
                return pAirline;
            }

            @Override
            public String getFlightNumber() {
                return null;
            }

            @Override
            public char getFlightNumberSuffix() {
                return 0;
            }

            @Override
            public Frequency getFrequency() {
                return null;
            }

            @Override
            public String getDeptArp() {
                return null;
            }

            @Override
            public String getArvlArp() {
                return null;
            }

            @Override
            public int getDistance() {
                return 0;
            }

            @Override
            public int getDeptTime() {
                return 0;
            }

            @Override
            public int getArvlTime() {
                return 0;
            }

            @Override
            public int getElapsedTime() {
                return 0;
            }

            @Override
            public boolean isOperational() {
                return pIsOperational;
            }

            @Override
            public String getOperationalAirline() {
                return pOperationalAirline;
            }

            @Override
            public String getOperationalFlightNumber() {
                return null;
            }
        };
    }
}