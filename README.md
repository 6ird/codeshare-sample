Codeshare Editing Example
================

The purpose of this example is to demonstrate the ability to edit out itineraries based on codeshare connections; similar to (but more thoroughly than) the 'Codeshare Connections Table' found in the Data Explorer.

## Types of Codeshare Connections:
![CodeshareConnections.png](https://bitbucket.org/repo/G96zGL/images/2309754159-CodeshareConnections.png)

## The Problem

When using the currently available codeshare connections table, if the 'Build Pure Nonop Online (Underlying Interline)' option is un-selected, during the connection builder stage, a connection such as:

WS\*/AA --- WS\*/DL

**would be edited out**.  However, a connection such as:

AS\*/AA --- AS --- AS\*/DL

**would still be built** because the Codeshare Connections table only prevents the first connection.  

This example demonstrates how to remove all itineraries that have a Pure Online (Underlying Interline) connection.